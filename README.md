# Pi-Weather

Weather Station built for Raspberry Pi Zero with a 2.7" PaPiRus E-Ink Display

## How to install

**1. First install requirements:**

```bash
#PaPiRus
curl -sSL https://pisupp.ly/papiruscode | sudo bash

#Weather API
sudo python3 -m pip install weather_au
```

More info on **PaPiRus** manual setup can be found on their [repository](https://www.github.com/PiSupply/PaPiRus).

**2. Modify config.json with your location**
```bash
$EDITOR config.json
```

**3. Run the python script**

```bash
python3 pi_weather.py
```

**4. You should get a result similar to this:**

![Image of Pi-Weather](/docs/result.jpg)


**5. Install the systemd service to start on boot**

```bash
sudo cp piweather.service /etc/systemd/system/
sudo systemctl daemon-reload
sudo systemctl enable pipweather.service
```
## Features

- Uses BOM API / Scraping (via weather_au pip module)
- Grabs a random fact each rotation

## Future Work

Replace PaPirusComposite with something else, so that we can Update specific sections when items change

## Debugging

BOM API Info: 
- https://api.weather.bom.gov.au/v1/locations?search=3000
- https://api.weather.bom.gov.au/v1/locations/$ID/forecasts/daily
